// JavaScript Document
//选择颜色增加去除
$(function () {
        $(".order-managememt-box button").click(
            function () {
                $(this).addClass("order-managememt-box-blue");
                $(this).siblings().removeClass("order-managememt-box-blue");
            }
        );
        $(".index-manage-classify .news-ba").click(
            function () {
                $(this).addClass("order-managememt-box-blue");
                $(this).siblings().removeClass("order-managememt-box-blue");
            }
        )
    }
);
//
$(function () {
        $(".unit-type-box").hide()
        $(".qualification-manage-two").click(
            function () {
                $(".work-type-box").hide();
                $(".operate-project-box").hide();
                $(".unit-type-box").show();
            }
        );
        $(".qualification-manage-one").click(
            function () {
                $(".work-type-box").show();
                $(".operate-project-box").show();
                $(".unit-type-box").hide();
            }
        )
    }
);
//


$(function () {
        $(".report-form li").click(
            function () {
                var line = $(this).data('child');
                $(this).addClass("report-list");
                $(this).siblings().removeClass("report-list");
                $(".reportform-box div[class*='active']").removeClass("active");
                $("#" + line).addClass("active");

            }
        )
    }
);
//培训对象对应选项变动
$(function () {
        $(".faculty-classify-box .news-ba").click(function () {
            if ($(this).hasClass("order-managememt-box-blue")) {
                $(this).removeClass("order-managememt-box-blue");
                if ($(this).hasClass("special-input-show")) {
                    $(".special-input").attr("disabled", "disabled");
                    $(".special-input").attr("checked", false);
                }
            } else {
                $(this).addClass("order-managememt-box-blue");
                if ($(this).hasClass("special-input-show")) {
                    $(".special-input").removeAttr("disabled");
                }
            }
            if ($(this).hasClass("special-input-all")) {
                $(this).siblings().removeClass("order-managememt-box-blue");
            } else {
                $(".special-input-all").removeClass("order-managememt-box-blue");
            }
        });

    }
);

function getCurrentMenu() {
    var menu = api.req.menu;

    if (menu != null && menu != undefined) {
        $("#" + menu).addClass("active");

        $(".manage").children("li").removeClass("active");
        $("#" + menu).addClass("active");
    } else {
        $("#TrainingPlanManagement").addClass("active");
    }
}

function showCurrentMenu() {
    var urlPath = window.location.href;
    if (urlPath.indexOf("courseManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#message_courseManage").addClass("active");
    }
    if (urlPath.indexOf("TrainingPlanManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#TrainingPlanManagement").addClass("active");
    }
    if (urlPath.indexOf("teachersAdministration") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#faculty").addClass("active");
    }
    if (urlPath.indexOf("videoManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#message_videoexamine").addClass("active");
    }
    if (urlPath.indexOf("NotificationManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#notice_manage").addClass("active");
    }
    if (urlPath.indexOf("PaymentManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#shop_payment").addClass("active");
    }
    if (urlPath.indexOf("OrderAdministration") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#shop_order_management").addClass("active");
    }
    if (urlPath.indexOf("VideoCourseStatistics") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#videoCount").addClass("active");
    }
    if (urlPath.indexOf("consumptionStatistics") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#count_consumeCount").addClass("active");
    }
    if (urlPath.indexOf("userManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#uesr_manage").addClass("active");
    }

    if (urlPath.indexOf("LogManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#system_log").addClass("active");
    }
    if (urlPath.indexOf("BasicInformation") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#basic_information").addClass("active");
    }
    if (urlPath.indexOf("CompleteProof") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#Complete_proof").addClass("active");
    }
    if (urlPath.indexOf("roleManagement") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#system_AuthorityRole").addClass("active");
    }
    if (urlPath.indexOf("TrainingPlanStatistics") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#count_planStatis").addClass("active");
    }
    if (urlPath.indexOf("StatisticsAdministration/count") != -1) {
        $(".manage").children("li").removeClass("active");
        $("#count_count").addClass("active");
    }

    if(urlPath.indexOf("BackupFile") != -1){
        $(".manage").children("li").removeClass("active");
        $("#backup_file").addClass("active");
    }
    if(urlPath.indexOf("system_config") != -1){
        $(".manage").children("li").removeClass("active");
        $("#system_config").addClass("active");
    }
}





