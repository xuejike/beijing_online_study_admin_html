// JavaScript Document


/*边栏子集移动显示隐藏*/
$(function () {
    $('body').on('mouseenter', '.has-children', function () {
		var id = $(this).data('child');
		//api.log(id);
		$('#' + id).removeClass("Sidebar-hide");
	})
    $('body').on('mouseleave', '.has-children', function () {
		var id = $(this).data('child');
        //api.log(id);
		$('#' + id).addClass("Sidebar-hide");
	})
});
$(function () {
    $('.sidebar-hide').mouseenter(
        function () {
            $(this).removeClass("Sidebar-hide");
        });
    $('.sidebar-hide').mouseleave(
        function () {
            $(this).addClass("Sidebar-hide");
        })
});
/*payment-button-change*/
function buttonclick(item) {
    item.toggleClass("payment-black-up-button payment-employ-button");
    if (item.text() == "停用")
        item.text("使用");
    else
        item.text("停用")
}
/*kecheng_students*/
function buttonclicks(qualified) {
    qualified.toggleClass("text-red text-blue");
    if (qualified.text() == "合格")
        qualified.text("不合格");
    else
        qualified.text("合格")
}
/*faculty*/
function recoveclick(recove) {
    recove.toggleClass("text-red text-blue");
    if (recove.text() == "开课") {
        recove.text("停用");
    }
    else {
        recove.text("开课")
    }
    post("", {}, function () {

    }, function () {

    })
}

/**/

/*添加讲师*/
$(function () {
        $(".add-type-hide-box").hide();
        $(".add-type").click(
            function () {
                $(".add-type-hide-box").show()
            }
        );
        $(".add-type-button").click(
            function () {
                $(".add-type-hide-box").hide()
            }
        )
    }
);
/*添加视频*/
$(function () {
        $(".add_video_box").hide();
        $(".addVideo-button").click(
            function () {
                $(".add_video_box").show();
                $(".confirm-button-box").hide()
            }
        );
        $(".add-type-button").click(
            function () {
                $(".add_video_box").hide();
                $(".confirm-button-box").show()
            }
        )
    }
)
/*课程评价-回复*/
$(function () {
        $(".hide-text-box").hide();
        $(".course-evaluation-particulars-amend").click(
            function () {
                $(".hide-text-box").show();
                $(".show-text-box").hide()
            }
        );
        /*$(".course-evaluation-particulars-submit").click(
         function(){
         $(".hide-text-box").hide();
         $(".show-text-box").show()
         }
         )*/
    }
);
/*点击习题弹窗*/
$(function () {
        $(".exercises-show-button").click(
            function () {
                $(".exercises-show-box").show();
            }
        );
        $(".exercises-hide").click(
            function () {
                $(".exercises-show-box").hide();
            }
        );
        $(".exercises-check-button").click(
            function () {
                $(".exercises-check-box").show();
            }
        );
        $(".exercises-hide").click(
            function () {
                $(".exercises-check-box").hide();
            }
        );



        /*培训计划管理添加计划*/
        $("#baseObjId").change(function () {

            var selectedValue = $(this).val();
            if (selectedValue == "sop") {
                $(".unit-type-hide").show();
                $(".unit-type-show").hide();
            }
            if (selectedValue == "spmp" || selectedValue == "prp") {
                $(".unit-type-hide").hide();
                $(".unit-type-show").show();
            }
        });
        /* 权限 */
        // 当input元素改变了状态
        $("body").on("change", ".authority-right input", function (e) {
            var $target = $(e.target),
                targetValue = $target.val(),
                parentValue = $target.data('parentvalue').toString() || '',     // 通过改变了状态的input的“dataparentid”属性 获取父级value
                checkedArr = sys_powerVm.checkedRes,                            // 所有选中了得id数组
                $parentInput = $("input[value='" + parentValue + "']"),         // 上级input
                grandParentValue = $parentInput.data("parentvalue").toString(),            // 上上级input的value
                parentFlag = false,     // 上级flag
                grandFlag = false;      // 上上级flag

            // 如果当前对象选中了，父级没有被选中则将父级value加入checkedArr[]，否则不做操作
            if ($target[0].checked == true && ($parentInput[0].checked == false)) {
                checkedArr.push(parentValue);
                // 上上级的value大于5为input 并且没有被选中
                ((grandParentValue >= 5) && (!$("input[value='" + grandParentValue + "']")[0].checked))
                    && checkedArr.push(grandParentValue);
            } else {
                var sameLvInputs = $("input[data-parentvalue='" + parentValue + "']");  // 同级input（不包括当前input）
                // 如果当前input没有被选中，检测所有其他统计对象是否被选中
                for (var i = 0; i < sameLvInputs.length; i ++) {                        // 同级的input存在被选中，则flag为true，父级不取消勾选
                    (sameLvInputs[i].checked) && (parentFlag = true);
                }
                if (!parentFlag && ($.inArray(parentValue, checkedArr) != -1)) {        // 如果都没有勾选，取消父级勾选
                    checkedArr.splice($.inArray(parentValue, checkedArr), 1);
                    // 取消了父级勾选后，判断父级的同级的check状态
                    var parentInputs = $("input[data-parentvalue='"+ $("input[value='" + grandParentValue + "']").val() +"']");
                    if (parentInputs.length > 0) {
                        for (i = 0; i < parentInputs.length; i ++) {
                            (parentInputs[i].checked) && (grandFlag = true);
                        }
                        // 如果父级的同级都没有被选中，取消上上级的勾选
                        (!grandFlag) && (($.inArray(grandParentValue, checkedArr) != -1) && checkedArr.splice($.inArray(grandParentValue, checkedArr), 1));
                    }
                }
            }
        });
    }
);

/*文本框改变*/

function noticeclick(textBoxConversion) {
    if (textBoxConversion.hasClass(".text-conversion-box")) {
        textBoxConversion.removeClass("text-conversion-box");
    }
    else {
        $(".text-float-left").addClass("text-conversion-box");
        textBoxConversion.removeClass("text-conversion-box");
    }
}
