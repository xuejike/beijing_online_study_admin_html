/**
 * Created by xuejike on 2016/12/14.
 */
var Bidanet;

(function (Bidanet) {
    var Permission = (function () {
        function Permission(config) {
            this.menusTag = "store_menus_tag";
            this.resourcesTag = "store_resources_tag";
            this.defaultConfig = {
                hideClassAttr: "permission-hide",
                hideClass: null,
                showClassAttr: "permission-show",
                showClass: null,
                scanClass: "bidanet-permission",
                resourcesAttr: "bidanet-resources"
            };
            this.config = $.extend(this.defaultConfig, config);
            this.loadCache();
            this.scan(document);
        }
        Permission.prototype.loadCache = function () {
            this.resources = store.get(this.resourcesTag);
            this.menus = store.get(this.menusTag);
        };
        Permission.prototype.setCache = function (tag, val) {
            store.set(tag, val);
        };
        /**
         * 加载菜单资源
         * @param menus
         */
        // loadMenus(menus:Array<Menu>){
        //     store.set(this.menusTag,menus);
        //     this.menus=menus;
        //     for(var i=0;i<menus.length;i++){
        //         var menu=menus[i];
        //         if(this.menusGroup[menu.group]==null){
        //             this.menusGroup[menu.group]=new Array<Menu>();
        //         }
        //         this.menusGroup[menu.group].push(menu);
        //     }
        // }
        /**
         * 加载权限资源
         * @param resource
         */
        Permission.prototype.loadResources = function (resource) {
            this.resources = new Object();
            for (var i = 0; i < resource.length; i++) {
                this.resources[resource[i]] = resource[i];
            }
            this.setCache(this.resourcesTag, this.resources);
        };
        /**
         * 扫描文档，显示拥有授权授权的元素
         * @param el
         */
        Permission.prototype.scan = function (el) {
            if(Utils.isNullEmpty(el)){
                el=document;
            }
            var checkEls = $(el).find("." + this.config.scanClass);
            for (var i = 0; i < checkEls.length; i++) {
                var cEl = checkEls[i];
                var res = $(cEl).attr(this.config.resourcesAttr);
                if (!Utils.isNullEmpty(res)) {
                    this.hasPermissionShow(res, cEl);
                    $(cEl).attr("ms-if","permission.hasPermission('"+res+"')")
                }

            }
        };
        /**
         * 判断是否有权限，有则显示
         * @param url url地址
         * @param el
         */
        Permission.prototype.hasPermissionShow = function (url, el) {
            if (this.hasPermission(url)) {
                this.showItem(el);
            }
            else {
                this.hideItem(el);
            }
        };
        /**
         * 显示
         * @param el
         */
        Permission.prototype.showItem = function (el) {
            var showClass = $(el).attr(this.config.showClassAttr);
            if (Utils.isNullEmpty(showClass)) {
                showClass = this.config.showClass;
            }
            if (Utils.isNullEmpty(showClass)) {
                $(el).show();
            }
            else {
                $(el).addClass(showClass);
            }
        };
        /**
         * 隐藏
         * @param el
         */
        Permission.prototype.hideItem = function (el) {
            var hideClass = $(el).attr(this.config.hideClassAttr);
            if (Utils.isNullEmpty(hideClass)) {
                hideClass = this.config.hideClass;
            }
            if (Utils.isNullEmpty(hideClass)) {
                $(el).hide();
            }
            else {
                $(el).addClass(hideClass);
            }
        };
        /**
         *是否有权限
         * @param url
         * @returns {boolean}
         */
        Permission.prototype.hasPermission = function (url) {
            try{
                var res = this.resources[url];
                if (res == null) {
                    return false;
                }
                else {
                    return true;
                }
            }catch(e) {
                //api.log("->"+url);
                return false;
            }

        };
        Permission.prototype.getAllMenu = function () {
        };
        return Permission;
    }());
    Bidanet.Permission = Permission;
    var Utils = (function () {
        function Utils() {
        }
        Utils.isNullEmpty = function (val) {
            if (val == null || val == undefined || val == "") {
                return true;
            }
            else {
                return false;
            }
        };
        return Utils;
    }());
    Bidanet.Utils = Utils;
})(Bidanet || (Bidanet = {}));
