/**
 * Created by Administrator on 2016/11/11.
 */



// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}


function dataStr(time){
    var time1 = new Date(time).Format("yyyy-MM-dd");
    return time1;
}

function dataStr2(time){

    var time2 = new Date(time).Format("yyyy-MM-dd hh:mm:ss");
    return time2;
}

function dataStr3(time){

    var time3 = new Date(time).Format("yyyy年MM月dd日");
    return time3;
}


function getValue(el){
    if(el == "" || el == undefined){
        return null;
    }
    return el;
}

function getIseEmpty(el){
    if(el == null){
        return "";
    }
    return el;
}

var pageInfo = new Object();



function getPageInfo(pageCurrent,pageSize,total){
    pageInfo.pageNo = pageCurrent;
    pageInfo.pageSize = pageSize;
    pageInfo.total = total;
    
    
}

function checkTotalClearPageTool(pageObj){
    if(pageObj.total == 0){
        $('#pageTool').empty();
    }
}

function pageTool(pageObj){

    $('#pageTool').empty();

    $('#pageTool').Paging({pagesize:  pageObj.pageSize ,count:  pageObj.total,
        callback:function(page,size,count){
            //api.log(arguments);
            /*        alert('当前第 ' +page +'页,每页 '+size+'条,总页数：'+count+'页');*/
            getPageInfo(page,size,pageObj.total);

            sendPost(getQueryParam(pageInfo),false);
        }});
}


/**
 * 返回上一页不刷新
 */
function goBack(){
    history.go(-1);
}








/**
 * 用户修改系列取值方法
 */

/*function getNationData(){
    post("admin/basicData/getNameTypeByType.do", {type:"民族"}, function (data) {
        api.log(data);
        sys_user_infotVm.dataNation = data;

    }, function (msg) {
        api.log(msg);
    });
}

function getRecordData(){
    post("admin/basicData/getNameTypeByType.do", {type:"学历"}, function (data) {
        api.log(data);
        sys_user_infotVm.dataRecord = data;

    }, function (msg) {
        api.log(msg);
    });
}

function getProvinceData(){
    var provinceData=api.storage.get("provinceData");
    if(provinceData==null||provinceData==undefined||provinceData==""){
    post("admin/basicData/getNameTypeByType.do", {type:"省"}, function (data) {
        api.log(data);
        sys_user_infotVm.dataProvince = data;
        api.storage.set("provinceData",data);
    }, function (msg) {
        api.log(msg);
    });
    }else{
        sys_user_infotVm.dataProvince = provinceData;
}
}*/



/*function getWorkCity(id){
    /!*if(id == null || id == undefined || id == ""){
        id = 0;
    }*!/
    post("admin/basicData/getCityNameType.do", {id:id}, function (data) {
        api.log(data);
        sys_user_infotVm.dataWorkCity=[];
        sys_user_infotVm.dataWorkCity = data;
    }, function (msg) {
        api.log(msg);
    });
}


function getContactCity(id){
    /!*if(id == null || id == undefined || id == ""){
        id = 0;
    }*!/
    post("admin/basicData/getCityNameType.do", {id:id}, function (data) {
        api.log(data);
        sys_user_infotVm.dataContactCity=[];
        sys_user_infotVm.dataContactCity = data;
    }, function (msg) {
        api.log(msg);
    });
}

function getCity(){

    post("admin/basicData/getNameTypeByType.do", {type:"市"}, function (data) {
        api.log(data);
        sys_user_infotVm.dataWorkCity = data;
        sys_user_infotVm.dataContactCity = data;
    }, function (msg) {
        api.log(msg);
    });

}


function getUserInfo(){
    var id = api.req.id;
    if(id != null){
        var param = {id:id}

        post("admin/user/getUserInfo.do",param,function (data) {
            api.log(data);
            if(data.birthDate==undefined||data.birthDate==''){
                data.birthDate=null;
            }
            sys_user_infotVm.data = data;

            if( sys_user_infotVm.data.addressBean==null|| sys_user_infotVm.data.addressBean==undefined|| sys_user_infotVm.data.addressBean==""){
                sys_user_infotVm.data.addressBean.address='';
                sys_user_infotVm.data.addressBean.addressCityId='';
                sys_user_infotVm.data.addressBean.addressProvinceId='';
            }
            if( sys_user_infotVm.data.workAddressBean==null|| sys_user_infotVm.data.workAddressBean==undefined|| sys_user_infotVm.data.workAddressBean==""){
                sys_user_infotVm.data.workAddressBean.address='';
                sys_user_infotVm.data.workAddressBean.addressCityId='';
                sys_user_infotVm.data.workAddressBean.addressProvinceId='';
            }
        },function (msg) {
            api.log(msg);
        });
    }
}


function getRoleData(){
    post("admin/role/getAllRole.do", {}, function (data) {
        api.log(data);
        sys_user_infotVm.dataRole = data;
    }, function (msg) {
        api.log(msg);
    });
}


var sys_user_infotVm = avalon.define({
    $id:"sys_user_info",
    data:[],
    dataNation:[],
    dataRecord:[],
    dataProvince:[],
    dataWorkCity:[],
    dataContactCity:[],
    dataRole:[],
    strTime:function(time){
        return dataStr(time);
    },
    workCityChange:function (e) {
        getWorkCity(e.target.value);
    },
    contactCityChange:function (e) {
        getContactCity(e.target.value);
    }

});*/


/*function getUserFormValue(){

    //获得姓名
    var username = $("#username").val();
    //获得性别
    var sex = $("input[name='sex']:checked").val();
    //获得学历
    var baseDataRecord = $("#baseDataRecord").val();
    //获得民族
    var baseDataNation = $("#baseDataNation").val();
    //获得出生日期
    var birthDate = $("#birthDate").val();
    //获得身份证号
    var idcard = $("#idcard").val();
    //获得当前手机号
    var mobile = $("#mobile").val();
    //获得当前邮箱
    var email = $("#email").val();
    //获得工作省份
    var workProvince = $("#workProvince").val();
    //获得工作城市
    var workAddressCityId = $("#workAddressCityId").val();
    //获得工作详细地址
    var workAddress = $("#workAddress").val();
    //获得通讯地址省份
    var contactProvince = $("#contactProvince").val();
    //获得通讯地址城市
    var addressCityId = $("#addressCityId").val();
    //获得通讯详细地址
    var address = $("#address").val();
    //获得角色id
    var strRoleIds = $("#strRoleIds").val();

    var workAddressDetail = (workProvince + "-" + workAddressCityId + "-" + workAddress);
    var addressDetail = (contactProvince + "-" + addressCityId + "-" + address);


    userParam.id = sys_user_infotVm.data.id;
    userParam.username = username;
    userParam.sex = sex;
    userParam.baseRecordId = baseDataRecord;
    userParam.baseNationId = baseDataNation;
    userParam.birthDate = birthDate;
    userParam.idcard = idcard;
    userParam.mobile = mobile;
    userParam.email = email;
    userParam.workAddress = workAddressDetail;
    userParam.address = addressDetail;

    if(strRoleIds != null && strRoleIds != undefined){
        userParam.strRoleIds = strRoleIds;
    }

}

var userParam = {
    id:null,
    username:null,
    sex:null,
    baseRecordId:null,
    baseNationId:null,
    birthDate:null,
    idcard:null,
    mobile:null,
    email:null,
    workAddress:null,
    address:null,
    strRoleIds:null
}*/




function saveUserInfo(){
    getUserFormValue();
    post("admin/user/getUserInfo.do",userParam,function (data) {
        //api.log(data);
    },function (msg) {
        //api.log(msg);
    });
}


//基础数据vm
var basicDataTypeVm = avalon.define({
    $id:"basicDataType",
    dataJobType:[],
    dataQuasiOperatione:[],
    dataExam:[],
    dataUnitType:[],
    getQuasiOperationIdByJobCategoryId:function (even) {
      var pid=even.target.value;

        //wanglu 2017-3-15

        if(pid == ""){
            //把准操项目清空
            basicDataTypeVm.dataQuasiOperatione = [];
        }else{
            getQuasiOperationIdByJobCategoryId(pid);
        }
    },
    getbaseObjId:function (even) {
        var pid=even.target.value;
        //wanglu 2017-5-9
        if(pid == ""){
            //把准操项目清空
            basicDataTypeVm.dataJobType = [];
            basicDataTypeVm.dataQuasiOperatione = [];
        }else{
            getQuasiOperationIdByJobCategoryId(pid);
        }
    }
});
//获取作业类别
function getJobTypeBasicData(){
    post("admin/basicData/getNameTypeByType.do",{type:"作业类别"},function (data) {
        //api.log(data);
        api.storage.set("dataJobType",data);
        basicDataTypeVm.dataJobType=[];
        basicDataTypeVm.dataJobType = data;
    },function (msg) {
        //api.log(msg);
    });
}
//级联获取准操作项目
function getQuasiOperationIdByJobCategoryId(pid) {
    post("admin/basicData/getQuasiOperationeByJobTypeBasicData.do",{pid:pid},function (data) {
        //api.log(data);
        basicDataTypeVm.dataQuasiOperatione=[];
        basicDataTypeVm.dataQuasiOperatione = data;

    },function (msg) {
        //api.log(msg);
    });
}

//获取准操项目数据
function getQuasiOperationeBasicData() {
    var dataQuasiOperatione = api.storage.get("dataQuasiOperatione")
    if (dataQuasiOperatione == null || dataQuasiOperatione == undefined) {
    post("admin/basicData/getNameTypeByType.do", {type: "准操项目"}, function (data) {
        //api.log(data);
        basicDataTypeVm.dataQuasiOperatione = [];
        basicDataTypeVm.dataQuasiOperatione = data;
        api.storage.set("dataQuasiOperatione", data);
    }, function (msg) {
        //api.log(msg);
    });
}else{
        basicDataTypeVm.dataQuasiOperatione = [];
        basicDataTypeVm.dataQuasiOperatione = dataQuasiOperatione;
    }
}
//获取考性质
function getExamBasicData(){
    post("admin/basicData/getNameTypeByType.do",{type:"考核性质"},function (data) {
        //api.log(data);
        api.storage.set("dataExam",data);
        basicDataTypeVm.dataExam=[];
        basicDataTypeVm.dataExam = data;
    },function (msg) {
        //api.log(msg);
    });
}
//获取单位类型
function getUnitTypeBasicData() {
    post("admin/basicData/getNameTypeByType.do",{type:"单位类型"},function (data) {
        //api.log(data);
        api.storage.set("dataUnitType",data);
        basicDataTypeVm.dataUnitType=[];
        basicDataTypeVm.dataUnitType = data;

    },function (msg) {
        //api.log(msg);
    });
}

//检查基础数据获得时间，如果不存在重新获取
function checkBasiceDataTime(){
   /* api.storage.clear();*/
    var basicDateTimeOfAdmin = api.storage.get("basicDateTimeOfAdmin");
    //如果取出的时间未定义，重新取一遍数据
    if(basicDateTimeOfAdmin == undefined) {
        addInitBasiceDataOfAdmin();
        return;
    }
    initBasiceDataOfAdmin();
}

//添加初始化数据
function addInitBasiceDataOfAdmin(){
    //获得作业类别数据
    getJobTypeBasicData();
    //获得考核性质数据
    getExamBasicData();
    //获取单位类型
    getUnitTypeBasicData();
    //存取当前取数据的时间
    api.storage.set("basicDateTimeOfAdmin",new Date().getTime());
}

//初始化加载基础数据
function initBasiceDataOfAdmin(){
    basicDataTypeVm.dataJobType =   api.storage.get("dataJobType");
    basicDataTypeVm.dataExam = api.storage.get("dataExam");
    basicDataTypeVm.dataUnitType =  api.storage.get("dataUnitType");
}









var teacher_typeVm = avalon.define({
    $id:"teacher_type",
    dataOccupationNameType:[],
    dataTrainFieldType:[]


});


function getTeacherBaseType() {

    post("admin/basicData/getNameTypeByType.do",{type:"职称"}, function(data){
        //api.log(data);
        teacher_typeVm.dataOccupationNameType = data;
    },function (msg) {
        //api.log(msg);
    });

    post("admin/basicData/getNameTypeByType.do",{type:"培训领域"}, function(data){
        //api.log(data);
        teacher_typeVm.dataTrainFieldType = data;
    },function (msg) {
        //api.log(msg);
    });

}


var facultyAddTeacherVm = avalon.define({
    $id:"facultyAddTeacher",
    cityChange:function (e) {
        getSelectedCity(e.target.value);
    },
    dataPositional:[],
    dataTraining:[],
    dataPoliticalStatus:[],
    dataNation:[],
    dataProvince:[],
    dataCity:[]

});

//获取培训领域和职称数据类型

function getAddTeacherBaseType(){
    post("admin/basicData/getNameTypeByType.do",{type:"职称"}, function(data){
        //api.log(data);
        facultyAddTeacherVm.dataPositional = data;
    },function (msg) {
        //api.log(msg);
    });

    post("admin/basicData/getNameTypeByType.do",{type:"培训领域"}, function(data){
        //api.log(data);
        facultyAddTeacherVm.dataTraining = data;
    },function (msg) {
        //api.log(msg);
    });

    post("admin/basicData/getNameTypeByType.do",{type:"政治面貌"}, function(data){
        //api.log(data);
        facultyAddTeacherVm.dataPoliticalStatus = data;
    },function (msg) {
        //api.log(msg);
    });

    post("admin/basicData/getNameTypeByType.do",{type:"民族"}, function(data){
        //api.log(data);
        facultyAddTeacherVm.dataNation = data;
    },function (msg) {
        //api.log(msg);
    });

    post("admin/basicData/getNameTypeByType.do", {type:"省"}, function (data) {
        //api.log(data);
        facultyAddTeacherVm.dataProvince = data;

    }, function (msg) {
        //api.log(msg);
    });

}

function getSelectedCity(id){
    /*if(id == null || id == undefined || id == ""){
        id = 0;
    }*/
    post("admin/basicData/getCityNameType.do", {id:id}, function (data) {
        //api.log(data);
        facultyAddTeacherVm.dataCity = data;
    }, function (msg) {
        //api.log(msg);
    });
}
var courseDetailsVm = avalon.define({
    $id:"courseDetails",
    courseData:[],
    dataCourseTag:[],
    delDataCourseTag:[],    //删除的tag保存数组
    removeCourseTag:function (index) {
        courseDetailsVm.dataCourseTag.courseTagList.removeAt(index);

        courseDetailsVm.dataCourseTag.courseTagIdList.removeAt(index);

    },
    data:{
        baseObjId:"",               //培训对象
        unitTypeId: "",             //单位类型
        jobCategoryId: "",          //作业类别
        quasiOperationId: "",       //准操项目
        examId: "",                  //考核性质
        objName:"",                 //培训对象显示名字
        unitTypeName:"",            //单位类型显示名称
        jobCategoryName:"",         //作业类别显示名称
        quasiOperationName:"",     //准操项目显示名称
        examName:"",                //考核性质显示名称
        allType:false,              //标识是否适用所有培训种类
    },
    addCourseBaseType:function() {

        if(!checkUpdateAddBaseData()){
            alert("请补充选择参数");
            return;
        }

        courseDetailsVm.courseData.allBaseType = false;

        getUpdateAddShowBaseData();

        getUpdateAddBaseData();

    },
    addAllBaseType:function () {
        $(".add-type-hide-box").hide();

         courseDetailsVm.courseData.allBaseType = true;

         //清空所有类型
        courseDetailsVm.dataCourseTag.courseTagIdList = [];
        courseDetailsVm.dataCourseTag.courseTagList = [];

    },
    removeAllBaseType:function () {
        courseDetailsVm.courseData.allBaseType = false;
    }
});


//检查选中的条件数据
function checkUpdateAddBaseData(){
    var flag = true;
    if(courseDetailsVm.data.baseObjId == ""){
        flag = false;
    }else{
        if(courseDetailsVm.data.baseObjId == "sop"){
            if(courseDetailsVm.data.jobCategoryId == ""){
                flag = false;
            }

            if(courseDetailsVm.data.quasiOperationId == ""){
                flag = false;
            }

            if(courseDetailsVm.data.examId == ""){
                flag = false;
            }
        }

        if(courseDetailsVm.data.baseObjId == "spmp"){

            if(courseDetailsVm.data.unitTypeId == ""){
                flag = false;
            }
            if(courseDetailsVm.data.examId == ""){
                flag = false;
            }

        }
        if(courseDetailsVm.data.baseObjId == "prp"){
            if(courseDetailsVm.data.unitTypeId == ""){
                flag = false;
            }
            if(courseDetailsVm.data.examId == ""){
                flag = false;
            }
        }
    }
    return flag;
}


//获得修改拼接的数据
function getUpdateAddBaseData(){
    var result = "";
    var resultName = "";

    if(courseDetailsVm.data.baseObjId == "sop"){
        var temp = (courseDetailsVm.data.baseObjId + "-" + courseDetailsVm.data.jobCategoryId
        + "," + courseDetailsVm.data.quasiOperationId
        + "," + courseDetailsVm.data.examId);

        result = temp;

        var name  = (courseDetailsVm.data.objName + "-"  + courseDetailsVm.data.jobCategoryName + "-"
        + courseDetailsVm.data.quasiOperationName + "-"
        + courseDetailsVm.data.examName);

        resultName = name;
    }
    if(courseDetailsVm.data.baseObjId == "spmp"){
        var temp = (courseDetailsVm.data.baseObjId + "-" + courseDetailsVm.data.unitTypeId
        + "," + courseDetailsVm.data.examId);
        result = temp;

        var name  = (courseDetailsVm.data.objName + "-"  + courseDetailsVm.data.unitTypeName + "-"
        + courseDetailsVm.data.examName);
        resultName = name;
    }
    if(courseDetailsVm.data.baseObjId == "prp"){
        var temp = (courseDetailsVm.data.baseObjId + "-" + courseDetailsVm.data.unitTypeId
        + "," + courseDetailsVm.data.examId);
        result = temp;

        var name  = (courseDetailsVm.data.objName + "-"  + courseDetailsVm.data.unitTypeName + "-"
        + courseDetailsVm.data.examName);
        resultName = name;
    }

    for (var i = 0; i < courseDetailsVm.dataCourseTag.courseTagIdList.length; i++){
        if(result == courseDetailsVm.dataCourseTag.courseTagIdList[i]){
            alert("不能添加重复项");
            return;
        }
    }

    //添加显示的数据
    courseDetailsVm.dataCourseTag.courseTagList.push(resultName);

    courseDetailsVm.dataCourseTag.courseTagIdList.push(result);
    /*alert(result);*/
}


//获得基础显示的值
function getUpdateAddShowBaseData(){
    courseDetailsVm.data.objName = $("#baseObjId").find("option:selected").text();
    courseDetailsVm.data.examName = $("#baseExamId").find("option:selected").text();
    courseDetailsVm.data.jobCategoryName = $("#baseJobCategoryId").find("option:selected").text();
    courseDetailsVm.data.quasiOperationName = $("#baseQuasiOperationId").find("option:selected").text();
    courseDetailsVm.data.unitTypeName = $("#baseUnitType").find("option:selected").text();
    //api.log("objName:"+courseDetailsVm.data.objName);
    //api.log("examName:"+courseDetailsVm.data.examName);
    //api.log("jobCategoryName:"+courseDetailsVm.data.jobCategoryName);
    //api.log("quasiOperationName:"+ courseDetailsVm.data.quasiOperationName);
    //api.log("unitTypeName:"+courseDetailsVm.data.unitTypeName);
}


//获取课程详情
function getCourseDetails() {
    var id = api.req.id;
    if (id != null && id != undefined) {
        post("admin/course/getCourse.do", {id: id}, function (data) {
            //api.log(data);
            courseDetailsVm.courseData = data;

        }, function (msg) {
            //api.log(msg);
        });
        //获得课程标签详情
        post("admin/course/getCourseTagList.do", {id: id}, function (data) {
            //api.log(data);
            courseDetailsVm.dataCourseTag = data;

        }, function (msg) {
            //api.log(msg);
        });
    }
}



/**
 * 检测是否登陆单机事件，如果登陆跳转到相应的地址，如果没有登陆，跳转到登陆页面
 * @param url
 */
function checkLoginStatusClick(url){
    var loginTime = api.storage.get("adminLoginTime");

    //如果登陆时间未定义或者当前时间大于30分钟就重新登陆
    if(loginTime == undefined || (new Date().getTime() - loginTime) > 1000*60*30){

        //未登录或者登陆超时，把当前url存起来
        api.storage.set("login_back_url",url);

        //跳转到登陆页面
        window.location.href = "/log-on.html";

        return;
    }

    window.location=url;

}


/**
 * 初始化检测登陆状态,返回boolean类型
 * @returns {boolean}
 */
function initCheckLoginStatus(){
    var loginTime = api.storage.get("adminLoginTime");
    //如果登陆时间未定义
    if(loginTime == undefined){
        return false;
    }

    return true;
}

/**
 * 保存用户信息
 * @param objData
 */
function saveLoginStatus(objData){
    //保存登陆用户信息
    api.storage.set("adminUserInfo",objData);
    api.storage.set("adminLoginTime",new Date().getTime());
}

/**
 * 获得用户信息
 */
function getLoginUser(){
    post("admin/userLogin/getUserInfo.do", {}, function (data) {
        //api.log(data);
        saveLoginStatus(data);
    }, function (msg) {
        alert(msg);
    });
}

avalon.directive('html', {
    parse: function (copy, src, binding) {
        /*
         copy: 每次VM的属性时,avalon就会调用vm.$render方法重新生成一个虚拟DOM树
         ,这个copy就是新虚拟DOM树的一个子孙节点

         src: 第一次调用vm.$render方法生成的虚拟DOM树的某个节点,它将永驻于内存中,
         除非其对应的真实节点被移除.以后不断用src与新生成的copy进行比较,
         然后应用 update方法,最后用copy的属性更新src与真实DOM.

         binding 配置对象,包括name,expr,type,param等配置项

         return 用于生成vtree的字符串 */
        //api.log("parse")
    },
    diff: function (copy, src, name) {
        /*
         copy 刚刚生成的虚拟DOM树的某个子孙节点
         src 最初的虚拟DOM树的节点
         name 要比较的指令名
         */
        //api.log("diff")
    },
    update: function (node, vnode, parent) {
        /*
         node 当前的真实节点
         vnode  此真实节点在虚拟树的相应位置对应的虚拟节点
         parent node.parentNode
         */
        //api.log("update")
    }
});
var permission;
$(document).ready(function(){
    // 在这里写你的代码...
    permission=new Bidanet.Permission();
});


/*//获得绑定的city
function getBindCity(){

    setTimeout(function () {
        getWorkCity($("#workProvince").val());
        getContactCity($("#contactProvince").val());
    },1000);
}*/


function checkquestionData(data){
    if (data.questionBanks.title == '' || data.questionBanks.title == undefined) {
        alert("习题标题不能为空")
        return false;
    }
    if (data.questionBanks.questions == '' || data.questionBanks.questions == undefined) {
        alert("习题不能为空")
        return false;
    }
    for(var i=0;i<data.questionBanks.questions.length;i++){
        var question=data.questionBanks.questions[i];
        if(question.title==''||
            question.type==''||
            question.answers.length<=0||
            question.options.length<=0) {
            alert("习题信息不完整")
            return false;
        }
    }
    return true;
}
