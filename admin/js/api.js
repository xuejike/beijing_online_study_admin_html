/**
 * Created by Administrator on 2016/7/2.
 */
var api=new Object();
/*var apiHost="http://192.168.1.146:8080/";*/
/*var apiHost="http://192.168.1.145:8080/";
/*var apiHost="http://192.168.1.156:8080/";*/
var apiHost="http://"+window.location.host+"/";
//var apiHost="http://192.168.1.142:8080/"
api.req= buildRequest();
api.debug=true;
$(function () {

});


function buildRequest() {
    var url = window.location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            //就是这句的问题
            theRequest[strs[i].split("=")[0]]=decodeURI(strs[i].split("=")[1]);
            //之前用了unescape()
            //才会出现乱码
        }
    }
    return theRequest;
}

function sendCallBack(data,successFun,errorFun,nologinFun) {
    if(data.statusCode==200){
        successFun(data.data);
    }else if(data.statusCode==300){

        errorFun(data.message);
    }else if(data.statusCode==301){
        // window.location.href="/home/user/login.do";
        if(nologinFun==undefined){
            window.location.href="/admin/index.html";
        }else{
            nologinFun();
        }
        nologinFun();
    }else if(data.statusCode==404) {
        alert(data.message)
    }else
    {
        errorFun("请求失败")
    }
}
var postOneArray={};
// var  permission =new Permission();
function post(path,param,successFun,errorFun,noLoginFun) {
    var md5Path=md5(path);
    if(!"admin/basicData/getNameTypeByType.do"==path){
        try{
            if(postOneArray[md5Path]!=undefined){
                //api.log("地址重复->"+path)
                return;
            }
        }catch (e){

        }
        postOneArray[md5Path]=path;
    }


    if(param==undefined){
        param={};
    }
    param.adminToken=api.storage.get("token");

    try {
        $.ajax({
            type: "POST",
            url: apiHost+path,
            data: param,
            dataType:"json",
            success: function(res){
                // res=$.parseJSON(res);
                // alert( "Data Saved: " + msg );
                delete postOneArray[md5Path];
                sendCallBack(res,successFun,errorFun,noLoginFun);
            },
            error:function () {
                delete postOneArray[md5Path];
            }
        });
    }catch (e){
        delete postOneArray[md5Path];
    }


//     $.getJSON(apiHost+path+"?jsonpcallback=?",param,
//         function(res){
// //                    api.log(data);
//             sendCallBack(res,successFun,errorFun,noLoginFun);
//
//         });
}

// function postOne(path,param,successFun,errorFun,noLoginFun){
//     if(postOneArray.indexOf(path)>=0){
//         return;
//     }else{
//         postOneArray.push(path);
//         post(path,param,function () {
//
//         },errorFun,noLoginFun);
//     }
// }
function postShowMark(path,param,successFun,errorFun,noLoginFun) {
    if(param==undefined){
        param={};
    }
    param.adminToken=api.storage.get("token");

    $.ajax({
        type: "POST",
        url: apiHost+path,
        data: param,
        dataType:"json",
        success: function(res){
            // res=$.parseJSON(res);
            // alert( "Data Saved: " + msg );
            sendCallBack(res,successFun,errorFun,noLoginFun);
        }
    });

}

function getMethod(path,param,successFun,errorFun,noLoginFun) {

    param.adminToken=api.storage.get("token");
    param.areaId=api.storage.get("areaId");
    $.getJSON(path+"?jsonpcallback=?",param,
        function(res){
//                    api.log(data);
            sendCallBack(res,successFun,errorFun,noLoginFun);

        });
}


function postCache(path, param, successFun, errorFun, noLoginFun,update) {
    var key=md5(path+"->"+JSON.stringify(param));
    // param.adminToken=api.storage.get("token");
    var cacheValue=api.cache.get(key);
    if(update==undefined){
        update=false;
    }
    if(cacheValue&&!update){
        successFun(cacheValue);
    }else{
        post(path,param,function (data) {
            api.cache.set(key,data);
            successFun(data);
        },errorFun,noLoginFun);
        // $.getJSON(apiHost+path+"?jsonpcallback=?",param,
        //     function(res){
        //         api.cache.set(key,res);
        //         sendCallBack(res,successFun,errorFun,noLoginFun);
        //
        //     });
    }
}

function saveLogin(data) {
    api.storage.set("token",data);
}

api.cache=new Object();
api.cache.set=function (key, val) {
    if(window.sessionStorage){  //或者 window.sessionStorage
        window.sessionStorage.setItem(key,JSON.stringify(val));
    }else{
        // alert("浏览暂不支持localStorage")
    }
};
api.cache.get=function (key) {
    if(window.sessionStorage){  //或者 window.sessionStorage
        return JSON.parse(window.sessionStorage.getItem(key));
    }else{
        // alert("浏览暂不支持localStorage")
    }
};
api.storage=new Object();
api.storage.set=function (key,val){
    // if(window.localStorage){  //或者 window.sessionStorage
    //     window.localStorage.setItem(key,JSON.stringify(val));
    // }else{
    //     // alert("浏览暂不支持localStorage")
    // }
    store.set(key,val);
};
api.storage.get=function (key) {
    // if(window.localStorage){  //或者 window.sessionStorage
    //     return JSON.parse(window.localStorage.getItem(key));
    // }else{
    //     // alert("浏览暂不支持localStorage")
    // }
    return store.get(key);
};

api.storage.remove=function (key) {
    store.remove(key);
}


api.storage.clear=function () {
    store.clear();
}

/*api.log=function (msg) {
    if(api.debug){
        if(console){
            console.log(msg);
        }

    }
}*/



function getQuery() {
    res={};
    var param=window.location.search.substr(1);
    param.split('&').forEach(function(i){
        var j = i.split('=');
        res[j[0]]=j[1];
    });
    return res;
}
function setQuery(param) {
    var p= $.param(param);
    window.location.hash=p;
}

/*initData();
function initData() {
    var updateTime=api.storage.get("loginStatusUpdateTime")
    //
    if(new Date().getTime()-updateTime>30*1000*60){
        //重新检测登陆
        api.storage.set("loginStatus",true);
    }
    api.storage.get("cc")

}

function checkLoginClick(url) {
    if(isLogin){
        window.location=url;
    }else{
        api.storage.set("login_back_url",window.location.href);
        window.location.href="";
    }
    // javascript:checkLoginClick('ddd');
}*/


function cc(url) {

}
function ccD() {
    //  <a class='checkAction' data-check-url='sss.html' href='sss.html?ddd=ddd'/>
    /*().show();*/
}

// $(document).on('click', 'a', function(event) {
//     var container = $(this).closest('[data-pjax-container]')
//     $.pjax.click(event, {container: ".slide-b"})
// })
//加载初始化数据，设置 数据有效期 10分钟
var loadBaseDataLimit=1000*60*10;

function initBaseData(loadAll) {
    if(loadAll==undefined){
        loadAll=false;
    }
    var baseTime= api.cache.get("initBaseDataTime");
    if(new Date().getTime()-baseTime>loadBaseDataLimit||loadAll){
        api.cache.set("initBaseDataTime",new Date().getTime())
        postCache("admin/basicData/getNameTypeByType.do", {type:"民族"}, function (data) {
            //api.log("学历加载成功");
            // student_user_reviseVm.dataNation = data;
        }, function (msg) {


        },function () {

        },true);
        postCache("admin/basicData/getNameTypeByType.do", {type:"学历"}, function (data) {
            // student_user_reviseVm.dataRecord = data;
            //api.log("学历加载成功");
        }, function (msg) {

        },function () {

        },true);

        postCache("admin/basicData/getNameTypeByType.do", {type:"省"}, function (data) {
            // student_user_reviseVm.dataProvince = data;
            //api.log("省加载成功");
        }, function (msg) {
            // api.log(msg);

        },function () {

        },true);
    }else{
        //api.log("使用缓存数据")
    }
}
api.setDefault=function (data,field, defaultValue) {
    // if(v==undefined){
    //     v=defaultValue;
    // }
    if(data[field]==undefined){
        data[field]=defaultValue;
    }
}

initBaseData();

